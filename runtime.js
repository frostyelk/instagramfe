/**
 Copyright 2015 Frosty Elk AB
 */
/* jshint -W069, -W097, -W117 */
// ECMAScript 5 strict mode
"use strict";

assert2(cr, "cr namespace not created");
assert2(cr.plugins_, "cr.plugins_ not created");

/////////////////////////////////////
// Plugin class
cr.plugins_.InstagramFE = function (runtime) {
    this.runtime = runtime;
};

//noinspection UnterminatedStatementJS
(function () {

    var pluginProto = cr.plugins_.InstagramFE.prototype;

    /////////////////////////////////////
    // Object type class
    pluginProto.Type = function (plugin) {
        this.plugin = plugin;
        this.runtime = this.plugin.runtime;
    };

    var typeProto = pluginProto.Type.prototype;

    // Plugin common vars
    var instagramReady = false;
    var instagramIsInstalled = false;
    var conditionTag = {};
    var instagramInst = {};
    var instagramRuntime = {};
    var lastError = "";
    var instagramPlugin = {};


    // called on startup for each object type
    typeProto.onCreate = function () {

    };

    /////////////////////////////////////
    // Instance class
    pluginProto.Instance = function (type) {
        this.type = type;
        this.runtime = this.type.runtime;
    };

    var instanceProto = pluginProto.Instance.prototype;

    function checkIfInstagramIsInstalled() {
        //console.log("checkIfInstagramIsInstalled()");

        if (!instagramReady) {
            return;
        }

        instagramPlugin["isInstalled"](function (err, installed) {
            if (installed) {
                console.log("Instagram is installed. Version: " + installed);
                instagramIsInstalled = true;
            } else {
                console.log("Instagram is not installed");
                instagramIsInstalled = false;
                lastError = err;
            }
        });
    }

    // called whenever an instance is created
    instanceProto.onCreate = function () {

        instagramInst = this;
        instagramRuntime = this.runtime;

        if (typeof window["Instagram"] === 'undefined') {
            console.log("Instagram FE not created, a supported Cordova plugin is missing");
            instagramReady = false;
        } else {
            console.log("Instagram FE running with Cordova Instagram plugin");
            instagramPlugin = window["Instagram"];
            instagramReady = true;
            checkIfInstagramIsInstalled();
        }

        document.addEventListener('resume', function () {
            console.log("Instagram FE resume: ");
            checkIfInstagramIsInstalled();
        });
    };


    // The comments around these functions ensure they are removed when exporting, since the
    // debugger code is no longer relevant after publishing.
    /**BEGIN-PREVIEWONLY**/
    instanceProto.getDebuggerValues = function (propsections) {
        // Append to propsections any debugger sections you want to appear.
        // Each section is an object with two members: "title" and "properties".
        // "properties" is an array of individual debugger properties to display
        // with their name and value, and some other optional settings.
        propsections.push({
            "title": "My debugger section",
            "properties": [
                // Each property entry can use the following values:
                // "name" (required): name of the property (must be unique within this section)
                // "value" (required): a boolean, number or string for the value
                // "html" (optional, default false): set to true to interpret the name and value
                //									 as HTML strings rather than simple plain text
                // "readonly" (optional, default false): set to true to disable editing the property

                // Example:
                // {"name": "My property", "value": this.myValue}
            ]
        });
    };

    instanceProto.onDebugValueEdited = function (header, name, value) {
        // Called when a non-readonly property has been edited in the debugger. Usually you only
        // will need 'name' (the property name) and 'value', but you can also use 'header' (the
        // header title for the section) to distinguish properties with the same name.
        /*
         if (name === "My property") {
         this.myProperty = value;
         }
         */

    };
    /**END-PREVIEWONLY**/

    //////////////////////////////////////
    // Conditions
    function Cnds() {
    }

    Cnds.prototype.IsReady = function () {
        return instagramReady;
    };

    Cnds.prototype.IsInstagramInstalled = function () {
        return instagramIsInstalled;
    };

    Cnds.prototype.onShareSuccessful = function (tag) {
        return cr.equals_nocase(tag, conditionTag.c2canvas);
    };

    Cnds.prototype.onShareFailed = function (tag) {
        return cr.equals_nocase(tag, conditionTag.c2canvas);
    };


    pluginProto.cnds = new Cnds();


    //////////////////////////////////////
    // Actions
    function Acts() {
    }

    Acts.prototype.doShareC2Canvas = function (tag, caption) {
        if (!instagramReady) {
            lastError = "Instagram not ready. Check condition 'Is Ready' before using any actions";
            console.warn(lastError);
            conditionTag.c2canvas = tag;
            instagramRuntime.trigger(cr.plugins_.InstagramFE.prototype.cnds.onShareFailed, instagramInst);
            return;
        }

        conditionTag.c2canvas = "";

        // Parameters needs to be null if not set
        var shareCaption = caption.trim() === "" ? null : caption;

        instagramPlugin["share"]("c2canvas", shareCaption, function (err) {
            if (err) {
                console.log("C2 Canvas not shared");
                conditionTag.c2canvas = tag;
                instagramRuntime.trigger(cr.plugins_.InstagramFE.prototype.cnds.onShareFailed, instagramInst);
            } else {
                console.log("C2 canvas shared");
                conditionTag.c2canvas = tag;
                instagramRuntime.trigger(cr.plugins_.InstagramFE.prototype.cnds.onShareSuccessful, instagramInst);
            }
        });
    };


    Acts.prototype.doShareSprite = function (tag, caption, gameObject) {
        if (!instagramReady) {
            lastError = "Instagram not ready. Check condition 'Is Ready' before using any actions";
            console.warn(lastError);
            conditionTag.c2canvas = tag;
            instagramRuntime.trigger(cr.plugins_.InstagramFE.prototype.cnds.onShareFailed, instagramInst);
            return;
        }

        conditionTag.c2canvas = "";

        // Parameters needs to be null if not set
        var shareCaption = caption.trim() === "" ? null : caption;

        // Get the current instance of the object
        var sol = gameObject.getCurrentSol();
        var instances = sol.getObjects();
        var obj = instances[0];

        if (!obj) {
            console.log("No instance of '" + gameObject.name + "' is available to create data from");
            return;
        }

        //noinspection JSUnresolvedVariable
        var image = obj.curFrame.texture_img;

        var canvasSprite = document.createElement("canvas");
        var ctxSprite = canvasSprite.getContext("2d");

        canvasSprite.width = image.width;
        canvasSprite.height = image.height;

        ctxSprite.drawImage(image, 0, 0);
        var dataURL = canvasSprite.toDataURL("image/png");

        instagramPlugin["share"](dataURL, shareCaption, function (err) {
            if (err) {
                console.log("Sprite not shared");
                conditionTag.c2canvas = tag;
                instagramRuntime.trigger(cr.plugins_.InstagramFE.prototype.cnds.onShareFailed, instagramInst);
            } else {
                console.log("Sprite shared");
                conditionTag.c2canvas = tag;
                instagramRuntime.trigger(cr.plugins_.InstagramFE.prototype.cnds.onShareSuccessful, instagramInst);
            }
        });
    };


    pluginProto.acts = new Acts();

    //////////////////////////////////////
    // Expressions
    function Exps() {
    }

    Exps.prototype.LastError = function (ret) {
        ret.set_string(lastError);
    };


    pluginProto.exps = new Exps();

}
());